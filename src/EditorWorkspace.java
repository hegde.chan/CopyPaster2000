import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

@SuppressWarnings("serial")

public class EditorWorkspace extends JPanel implements MouseListener, MouseMotionListener{ 
	//EditorPanel: Level editing workspace upon which image and grid are drawn
	private static final int TILE_SIZE = 32;
	private static final int ALPHA = 63; //For highlighting tiles with a transparent rect
	private BufferedImage tileMap;
	private int[][] tileValues;
	public boolean save = true;
	private Point currentTile; //Tiles represented as points
	private ArrayList<Point> selectedTiles = new ArrayList<>();
	public EditorWorkspace(int[][] tileValues){
		this.tileValues = tileValues;
		System.out.println(this.tileValues[0]);
		this.setFocusable(true);
	}
	
	public void setTileMap(BufferedImage tm){
		this.tileMap = tm;
		currentTile = new Point(0,0);
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
	}
	
	
	@Override
	public void paintComponent(Graphics g){ 
		super.paintComponent(g);
		g.drawImage(tileMap, 0, 0, null);
		drawGrid(g);
		highlightTiles(g);
	}
	
	private void drawGrid(Graphics g){
		for(int i = 0; i < tileMap.getWidth(); i+= TILE_SIZE){
			g.drawLine(i, 0, i, i + tileMap.getHeight());
		}
		for(int j = 0; j < tileMap.getHeight(); j+= TILE_SIZE){
			g.drawLine(0, j, tileMap.getWidth(), j);
		}
	}
	
	public void highlightTiles(Graphics g){
		Color yellow = new Color(255, 199, 0, ALPHA);
		g.setColor(yellow);
		g.fillRect(currentTile.x * TILE_SIZE, currentTile.y * TILE_SIZE, TILE_SIZE, TILE_SIZE);
		for(Point tile: selectedTiles){
			g.fillRect(tile.x * TILE_SIZE, tile.y * TILE_SIZE, TILE_SIZE, TILE_SIZE);
		}
	}
	
	public void save(File file, String imgPath){
		try{
			DataOutputStream out = new DataOutputStream(new FileOutputStream(file));
			out.writeUTF("res/" + imgPath.substring(imgPath.lastIndexOf('/') + 1)); 
			//Use image in to res/(filename of image); this means editor must have a res/ folder where the images are stored 
			out.writeUTF("\n");
			out.writeUTF(tileValues.length + "");
			out.writeUTF("\n");
			out.writeUTF(tileValues[0].length + "");
			out.writeUTF("\n");
			for(int[] i: tileValues){
				for(int j: i){
					out.writeUTF(j + "");
				}
				out.writeUTF("\n");
			}
			out.close();
			save = true;
		}
		catch(IOException ex){
			JOptionPane.showMessageDialog(this, "Unable to write file");
		}
	}
	
	public int[][] getTileValues(){
		return tileValues;
	}
	

	@Override
	public void mouseClicked(MouseEvent e) {
		if(tileMap == null || e.getClickCount() < 2) return;
		String choice = null;
		try{
			choice = JOptionPane.showInputDialog("Tile ID Value: ", tileValues[e.getX()/TILE_SIZE][e.getY()/TILE_SIZE]);
			tileValues[e.getX()/TILE_SIZE][e.getY()/TILE_SIZE] = Integer.parseInt(choice);
			save = false;
		}
		catch(NumberFormatException ex){
			if(choice == null) return;
			JOptionPane.showMessageDialog(this, "Invalid tile value");
		}
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		currentTile.setLocation(e.getX()/TILE_SIZE, e.getY()/TILE_SIZE);
		repaint();
		//currentTile = (e.getY()/TILE_SIZE * tileMap.getWidth()/TILE_SIZE) + e.getX()/TILE_SIZE; 
		//currentTile is represented by its placement in the tilemap, not by its coordinates
		//e.g. the tile at (1,1) will be 33 (assuming each row is 32 tiles long)
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		Point tile = new Point(e.getX()/TILE_SIZE, e.getY()/TILE_SIZE);
		if(selectedTiles.contains(tile)) return;
		selectedTiles.add(tile);
		repaint();
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		String choice = null;
		try{
			choice = JOptionPane.showInputDialog("Tile ID Value: ", new Integer(0));
			for(Point tile: selectedTiles){
				tileValues[tile.x][tile.y] = Integer.parseInt(choice);
			}
			save = false;
		}
		catch(NumberFormatException ex){
			if(choice == null) return;
			JOptionPane.showMessageDialog(this, "Invalid tile value");
		}
		selectedTiles.clear();
		repaint();
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	

	

	
}
