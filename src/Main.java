import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public final class Main {
	private static JFrame mainFrame;
	private static BufferedImage workingImage;
	private static EditorWorkspace workspace;
	private static String imagePath;
	private static int[][] tileValues;
	private static final int TILE_SIZE = 32;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		newFileChooser();
		workspace = new EditorWorkspace(tileValues);
		createFrame();
		workspace.setTileMap(workingImage);
		workspace.repaint();
	}
	
	private static void createFrame(){
		mainFrame = new JFrame("CopyPaster 2000 (v1.0)");
		mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		mainFrame.setSize(workingImage.getWidth(), workingImage.getHeight());
		
		drawMenuBar();
		mainFrame.setContentPane(workspace);
		mainFrame.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e){
				if(workspace.save){
					System.exit(0);
				}
				String[] options = {"Yes", "Cancel"};
				int prompt = JOptionPane.showOptionDialog(mainFrame, "Quit without saving?", "Quit", JOptionPane.OK_CANCEL_OPTION,
						JOptionPane.WARNING_MESSAGE, null, options, JOptionPane.CANCEL_OPTION);
				if(prompt == JOptionPane.OK_OPTION) System.exit(0);
			}
		});
		mainFrame.setVisible(true);
	}
	
	private static void drawMenuBar(){
		JMenuBar menuBar = new JMenuBar();
		JMenu file = new JMenu("File");
		JMenuItem newLevel = new JMenuItem("New Level From Image");
		JMenuItem openLevel = new JMenuItem("Open Level");
		JMenuItem saveLevel = new JMenuItem("Save Level");
		newLevel.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				newFileChooser();
			}
		});
		openLevel.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				
			}
		});
		saveLevel.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				saveFileChooser();
			}
		});
		file.add(newLevel);
		file.add(saveLevel);
		menuBar.add(file);
		
		mainFrame.setJMenuBar(menuBar);
	}

	private static void newFileChooser(){
		//Set up file chooser
		JFileChooser openImg = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("PNG/LVL Files", "png", "lvl");
		openImg.setFileFilter(filter);
		openImg.setCurrentDirectory(
				new File(System.getProperty("user.dir")));
		//Process selected file
		int okay = openImg.showOpenDialog(mainFrame);
		if(okay == JFileChooser.APPROVE_OPTION){
			File file = openImg.getSelectedFile();
			String extension = file.getName().substring(file.getName().lastIndexOf('.') + 1);
			if(extension.toUpperCase().equals("PNG")){
				try{
					workingImage = ImageIO.read(file);
					imagePath = file.getPath();
					tileValues = new int[workingImage.getHeight()/TILE_SIZE][workingImage.getWidth()/TILE_SIZE];
				}
				catch(IOException e){
					JOptionPane.showMessageDialog(mainFrame, "Error reading file.");
				}
			}
			else if(extension.toUpperCase().equals("LVL")){
				try{
					DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));
					while(in.available() > 0){
						imagePath = in.readUTF();
						workingImage = ImageIO.read(new File(imagePath));
						in.readUTF();
						int l = Integer.parseInt(in.readUTF());
						in.readUTF();
						int w = Integer.parseInt(in.readUTF());
						in.readUTF();
						tileValues = new int[l][w];
						int row = 0, col = 0;
						while(row < l){
							String next = in.readUTF();
							if(next.equals("\n")){
								row++;
								col = 0;
							}
							else{
								tileValues[row][col] = Integer.parseInt(next);
								col ++;
							}
						}
					}
					in.close();
				}
				catch(IOException ex){
					ex.printStackTrace();
					JOptionPane.showMessageDialog(mainFrame, "Unable to open file");
				}
			}
		}
	}
	
	private static void saveFileChooser(){
		JFileChooser saveImg = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("LVL Files", "lvl");
		saveImg.setFileFilter(filter);
		int okay = saveImg.showSaveDialog(mainFrame);
		if(okay == JFileChooser.APPROVE_OPTION){
			workspace.save(saveImg.getSelectedFile(), imagePath);
		}
	}
}
